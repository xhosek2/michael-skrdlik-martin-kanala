# Who The Hack Is It Discord bot

## Prerequisites
1. Install requirements
```bash
$ pip install -r requirements.txt
```
2. Allow MongoDB access for your IP address
3. Update values `.env` if needed

## How to run it locally
```bash
$ python main.py
```

## List of commands
The bot is capable to execute commands described below. In the Discord channel use this command as a new message with attached photo (JPG or PNG).

### How many faces
`!how_many_faces`:
Returns a count of the faces in the picture.
> **NOTE**: Picture might contain more than one face.

### Who is it
`!who_is_it`:
Returns an information about the person in the picture from the 'known-people' database.
> **NOTE**: Picture must contain only one face.

### Add person
`!add_person`:
Adds the person into the well-known database. Additional information (birth date, nationality and gender) can be fetched from the [Celebrity API](https://api-ninjas.com/api/celebrity) or manually by the user.
> **NOTE**: Picture must contain only one face.